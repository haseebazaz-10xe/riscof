#################
Welcome to RISCOF 
#################

.. toctree::
   :glob:
   :maxdepth: 2
   :numbered:

   intro
   overview
   installation
   plugins
   testlist
   database
   newtest
   coverage
   testformat
   RISCV-Config [External] <https://riscv-config.readthedocs.io/>
   code-doc
   changelog
